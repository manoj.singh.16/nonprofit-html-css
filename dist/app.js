const form = document.querySelector('form');
const error = document.getElementById('error');

const inputs = document.querySelectorAll('input');

const firstname = document.getElementById('firstname');
const lastname = document.getElementById('lastname');
const address = document.getElementById('address');
const city = document.getElementById('city');
const state = document.getElementById('state');
const zipcode = document.getElementById('zipcode');
const email = document.getElementById('email');
const ccnum = document.getElementById('ccnum');
const expMonth = document.getElementById('exp');
const expYear = document.getElementById('date');
const securitycode = document.getElementById('securitycode');

const isEmpty = (value) => value === '' || value === undefined || value === 'default';

const minMax = (value, min, max) => value.length >= min && value.length <= max;

const validateFirstName = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'First name is required';
  } else if (!minMax(value, 3, 15)) {
    error = 'First name should be between 3 to 15 characters';
  }

  return { error, isValid: isEmpty(error) };
};

const validateLastName = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'Last name is required';
  } else if (!minMax(value, 3, 15)) {
    error = 'Last name should be between 3 to 15 characters';
  }

  return { error, isValid: isEmpty(error) };
};

const validateAddress = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'Address is required';
  }

  return { error, isValid: isEmpty(error) };
};

const validateCity = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'City is required';
  }

  return { error, isValid: isEmpty(error) };
};

const validateState = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'State is required';
  }

  return { error, isValid: isEmpty(error) };
};

const validateZip = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'Zip is required';
  } else if (!Number.isInteger(parseInt(value))) {
    error = 'Zip code must be a number';
  }

  return { error, isValid: isEmpty(error) };
};

const validateEmail = (value) => {
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let error;
  if (isEmpty(value)) {
    error = 'Email is required';
  } else if (!regex.test(value)) {
    error = 'Invalid Email: email should be this format john@test.com';
  }

  return { error, isValid: isEmpty(error) };
};

const validateCCNum = (value) => {
  const regex =
    /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/;
  let error;
  if (isEmpty(value)) {
    error = 'credit card number is required';
  } else if (!regex.test(value)) {
    error = 'Invalid Credit card number';
  }

  return { error, isValid: isEmpty(error) };
};

const validateExpMonth = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'Expiration month is required';
  } else if (!Number.isInteger(parseInt(value))) {
    error = 'Expiration month should be a number';
  } else if (parseInt(expMonth.value) < 1 || parseInt(expMonth.value) > 12) {
    error = 'Expiration month should be between 01 and 12';
  }

  return { error, isValid: isEmpty(error) };
};

const validateExpYear = (value) => {
  let error;
  if (isEmpty(value)) {
    error = 'Expiration Year is required';
  } else if (!Number.isInteger(parseInt(value))) {
    error = 'Expiration Year should be a number';
  } else if (!minMax(value, 4, 4)) {
    error = 'Expiration Year should be in format 1996';
  }

  return { error, isValid: isEmpty(error) };
};

const validateCVV = (value) => {
  const regex = /^[0-9]{3,4}$/;
  let error;
  if (isEmpty(value)) {
    error = 'Security code is required';
  } else if (!regex.test(value)) {
    error = 'Security code should be a number and between 3 to 4 digits';
  }

  return { error, isValid: isEmpty(error) };
};

form.addEventListener('submit', (e) => {
  const isValidFirstname = validateFirstName(firstname.value);
  const isValidLastname = validateLastName(lastname.value);
  const isValidAddress = validateAddress(address.value);
  const isValidCity = validateCity(city.value);
  const isValidState = validateState(state.value);
  const isValidZip = validateZip(zipcode.value);
  const isValidEmail = validateEmail(email.value);
  const isValidCCNum = validateCCNum(ccnum.value);
  const isValidExpMonth = validateExpMonth(expMonth.value);
  const isValidExpYear = validateExpYear(expYear.value);
  const isValidCVV = validateCVV(securitycode.value);

  if (!isValidFirstname.isValid) {
    error.innerText = isValidFirstname.error;
    firstname.focus();
  } else if (!isValidLastname.isValid) {
    error.innerText = isValidLastname.error;
    lastname.focus();
  } else if (!isValidAddress.isValid) {
    error.innerText = isValidAddress.error;
    address.focus();
  } else if (!isValidCity.isValid) {
    error.innerText = isValidCity.error;
    city.focus();
  } else if (!isValidState.isValid) {
    error.innerText = isValidState.error;
    state.focus();
  } else if (!isValidZip.isValid) {
    error.innerText = isValidZip.error;
    zipcode.focus();
  } else if (!isValidEmail.isValid) {
    error.innerText = isValidEmail.error;
    email.focus();
  } else if (!isValidCCNum.isValid) {
    error.innerText = isValidCCNum.error;
    ccnum.focus();
  } else if (!isValidExpMonth.isValid) {
    error.innerText = isValidExpMonth.error;
    expMonth.focus();
  } else if (!isValidExpYear.isValid) {
    error.innerText = isValidExpYear.error;
    expYear.focus();
  } else if (!isValidCVV.isValid) {
    error.innerText = isValidCVV.error;
    securitycode.focus();
  } else {
    return;
  }

  e.preventDefault();
});
